<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
<div class="container">
    <table class="table table-bordered table-striped table-hover">
        <thead class="thead bg-info text-white ">
            <th>SL</th>
            <th>Name</th>
        </thead>
        <tbody>

            <?php

                $students = ['arif','abbas','jishan','jahir'];

            foreach ($students as $index => $student):

            ?>
            <tr>
                <td><?= $index+1; ?></td>
                <td><?= $student?></td>
            </tr>
            <?php
                endforeach;
            ?>
        </tbody>
    </table>
</div>

</body>
</html>
