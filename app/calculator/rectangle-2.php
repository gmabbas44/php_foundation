<?php 	

	class Rectangle 
	{
		
		public $width;
		public $height;
		public $length;

		public function getArea(){
			return ($this->width*$this->height*$this->length);
		}
		public function setWidth($width){
			$this->width = $width;
		}
		public function setHeight($height){
			$this->height = $height;
		}
		public function setLength($length){
			$this->length = $length;
		}
	}

	$area = new Rectangle();
	$area -> setWidth(10);
	$area -> setHeight(20);
	$area -> setLength(20);
	echo $area -> getArea();


 ?>