<?php

//    a.	echo $x;
//    b.	echo "<br/>";
//    c.	echo $x+++$x++;
//    d.	echo "<br/>";
//    e.	echo $x;
//    f.	echo "<br/>";
//    g.	echo $x---$x--;
//    h.	echo "<br/>";
//    i.	echo $x;


    $x = 5;
    echo "This output is variable 'x' " . $x; //output 5;
    echo "<br/>";
//        5 + 6
    echo $x+++$x++; // 5 + 6 = 11;
    echo "<br/>";
    echo "increment counter ".$x++; //output 7;
    echo "<br/>";
    echo $x; // output 8;
    echo "<br/>";
    echo $x---$x--; // 8 - 7 = 1 output 1
    echo "<br/>";
    echo $x; // output 6



?>