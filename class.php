<?php 

	class Car
	{
		public $door = '';
		public $seat = '';
		
		public function drive()
		{
			echo 'drive';
		}
	}

	class Fruit
	{
		public $color = '';
		public $taste = '';

		public function isSweet()
		{
			echo 'Fruit';
		}
		
	}

	class Course
	{
		public $title = '';
		public $credit = '';

		public function hasOffered()
		{
			echo 'Web App development PHP';
		}
		
	}

$car = new Car();
$fruit = new Fruit();
$course = new Course();

$car -> drive();
echo '<br>';
$fruit -> isSweet();
echo '<br>';
$course -> hasOffered();



 ?>