<?php 	

	class Triangle 
	{
		
		public $base;
		public $height;

		public function getArea(){
			return ($this->base*$this->height)/2;
		}
		public function setBase($base){
			$this->base = $base;
		}
		public function setHeight($height){
			$this->height = $height;
		}
	}

	$area = new Triangle();
	$area -> setBase(10);
	$area -> setHeight(20);
	echo $area -> getArea();


 ?>