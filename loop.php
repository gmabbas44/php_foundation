<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
</head>
<body>

    <?php
        $students = ['arif','abbas','jishan','jahir'];
        $count = count($students);
        echo $students[0].'<br />';
        echo $students[1].'<br />';
        echo $students[2].'<br />';
        echo $students[3].'<br />';

        echo '<hr />';
        echo 'used foreach loop';
        echo '<ul>';
        foreach ($students as $student){
            echo "<li> $student </li>";
        }
        echo '</ul>';

        echo '<hr />';
        echo 'used for loop';
        echo '<ul>';
        for ($increment = 0; $increment < $count; $increment++)
        {
            echo "<li> $students[$increment] </li>";
        }
        echo '</ul>';
    ?>

    <ul>
        <li><?= $students[0]; ?></li>
        <li><?= $students[1]; ?></li>
        <li><?= $students[2]; ?></li>
        <li><?= $students[3]; ?></li>
    </ul>
</body>
</html>
