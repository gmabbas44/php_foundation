<?php 	

	class Circle 
	{
		
		public $radius;
		public const PI = 3.1416;

		public function getRadius(){
			return ($this->radius*$this->radius*self::PI)/2;
		}
		public function setRadius($radius){
			$this->radius = $radius;
		}
	}

	$area = new Circle();
	$area -> setRadius(10);
	echo $area -> getRadius();


 ?>