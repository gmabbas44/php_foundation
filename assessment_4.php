<?php

//4.  var_dump(0123 == 123);
//    var_dump('0123' == 123);
//    var_dump('0123' === 123);


var_dump(0123 == 123); // 0123 is integer value is 83 and 123 is integer value is 123, they are not same or equal. so boolen return false
var_dump('0123' == 123); // '0123' this data type is string and 123 datatype is integer, but they are value is same. so boolen return true
var_dump('0123' === 123); // '0123' this data type is string and 123 datatype is integer, but they are not identical. so boolen return false.


