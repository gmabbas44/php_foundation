<?php

//
//A cookie is a bit of data stored by the browser and sent to the server with every request.
//
//A session is a collection of data stored on the server and associated with a given user.