<?php 	

	class Rectangle 
	{
		
		public $width;
		public $height;

		public function getArea(){
			return ($this->width*$this->height);
		}
		public function setWidth($width){
			$this->width = $width;
		}
		public function setHeight($height){
			$this->height = $height;
		}
	}

	$area = new Rectangle();
	$area -> setWidth(10);
	$area -> setHeight(20);
	echo $area -> getArea();


 ?>