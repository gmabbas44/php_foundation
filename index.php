<?php 

	// function __autoload($missingfullpath){
		
	// 	$namespace = $missingfullpath;
	// 	$fullPath = str_replace('\\', '/', $namespace);
	// 	include_once($fullPath.'.php');
		
	// }
	include_once('vendor/autoload.php');  // composer auto load
	// include_once('calculator/circle.php');
	// include_once('calculator/rectangle.php');
	// include_once('calculator/triangle.php');

	use \app\calculator\Circle;
	use \app\calculator\Rectangle;
	use \app\calculator\Triangle;

	echo 'Circle <br>';
	$circle = new Circle();
	$circle -> setRadius(10);
	echo $circle -> getRadius();

	echo '<hr />';

	$rectangle = new Rectangle();
	$rectangle -> setWidth(10);
	$rectangle -> setHeight(20);
	echo $rectangle -> getArea();

	echo '<hr />';

	$triangle = new Triangle();
	$triangle -> setBase(10);
	$triangle -> setHeight(20);
	echo $triangle -> getArea();

	echo '<hr />';




 ?>